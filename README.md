# UI/UX Coding Challenge

## Introduction

Here at PebblePost, we are growing super fast and need to quickly create new, responsive, and intuitive dashboards for our internal and external users.

For this coding challenge, we would like you to take an open set of business requirements, think on your feet, and quickly prototype and create a dashboard that satisfies these requirements.

## Business Requirements

### Design and Build a 3-Section Form using your choice of Front-End Framework (we use React, by the way)
* Form Fields
    1. Basic Information
        1. Name
        2. Address
        3. City
        4. State
        5. Zip
    2. Favorite Things
        1. Color
        2. Food
        3. Movie
    3. Poll Question
        1. What is your favorite season?
* Store the results of the form

## Expectations

We have a few goals we'd like you to keep in mind while undertaking this task:

* We are assessing your ability to rapidly prototype from a set of very general requirements.
   - Try not to get caught in the details
* Think through and be able to express your design decisions

## Business Requirements

### Basic
* You have your choice of front-end frameworks, so use one you know best and try to use the framework's best practices
* Style counts. Lay out the form fields and sections with best UX practice in mind.
* You must display some sort of progress bar
* You must store the results of the form; though format is at your discretion

### Optional, But Highly Encouraged
* Make tasteful use of a UI framework like Bootstrap or Foundation
* Make the page responsive
* Use any external APIs or components you'd like

## Final Note

We understand the time pressure and stresses of this challenge and do not expect 100% tested, production-ready code. Nonetheless, we expect you to try your best to write clean code, make use of comments, and be able to explain your design decisions, as well as what additional changes you would make if you had more time and/or had additional developer resources to leverage to bring this prototype to life.

We will also be available for questions throughout. Any/All questions are highly encouraged.
